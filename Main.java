package HierarchyFirure;

import HierarchyFirure.figure.Figure;

public class Main {
      public static void main(String[] args) {
            Figure[] figure = {
                (Figure) new Rectangle(8, 12),
                (Figure) new Circle(10)
            };
            for (Figure fig: figure) System.out.println(fig.getName() + ": Площадь = " + fig.getArea());
        }
    }